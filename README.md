# Home library #

Simple web application that allows:

* add books
* delete books
* edit books
* view details of book
* serach for book by title or author

The WAR file that enables deployment to application server can be downloaded from here: [https://goo.gl/K4LRLc](https://goo.gl/K4LRLc)

After deployment connect to:

```
#!text
http://localhost:8080/library
```

Log in with:

```
#!text
username: user
password: user123
```

What was not done because of lack of time:

* database using ORM :<
* unit tests
* downloading book covers automatically when adding books
* various filters