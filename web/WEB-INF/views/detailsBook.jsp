<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"/>
</head>
<body>
    <div class="jumbotron">
        <div class="container text-center">
            <h1>Home library</h1>
            <p>Details</p>
        </div>
    </div><br>

    <div class="container">
        <legend><h1><b>"${book.title}"</b></h1></legend>
    </div>

    <div class="container">
        <div class="form-group">
            <div class="col-lg-10">
                <img id="bookCover" src="/resource/images/${book.id}.jpg" class="img-responsive" alt="Image" height="200" width="100">
            </div>
        </div>
    </div><br>

    <div class="container">
        <fieldset>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="bookTitle">
                    <spring:message code="addBook.form.title.label"/>
                </label>
                <div class="col-lg-10">
                    <p id="bookTitle">${book.title}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="bookAuthor">
                    <spring:message code="addBook.form.author.label"/>
                </label>
                <div class="col-lg-10">
                    <p id="bookAuthor">${book.author}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="bookPublisher">
                    <spring:message code="addBook.form.publisher.label"/>
                </label>
                <div class="col-lg-10">
                    <p id="bookPublisher">${book.publisher}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="bookISBN">
                    <spring:message code="addBook.form.isbn.label"/>
                </label>
                <div class="col-lg-10">
                    <p id="bookISBN">${book.ISBN}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="bookCategory">
                    <spring:message code="addBook.form.category.label"/>
                </label>
                <div class="col-lg-10">
                    <p id="bookCategory">${book.category}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="bookReleaseDate">
                    <spring:message code="addBook.form.date.label"/>
                </label>
                <div class="col-lg-10">
                    <p id="bookReleaseDate">${book.releaseDate}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="bookNumberOfPages">
                    <spring:message code="addBook.form.pages.label"/>
                </label>
                <div class="col-lg-10">
                    <p id="bookNumberOfPages">${book.numberOfPages}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="bookLanguage">
                    <spring:message code="addBook.form.language.label"/>
                </label>
                <div class="col-lg-10">
                    <p id="bookLanguage">${book.language}</p>
                </div>
            </div>
            <div class="form-group">
                <a href=" <spring:url value="/library" />" class="btn btn-danger">
                    Back
                </a>
            </div>
        </fieldset>
    </div><br>

</body>
</html>


