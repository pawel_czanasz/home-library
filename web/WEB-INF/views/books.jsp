<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body>

    <div class="jumbotron">
        <div class="container text-center">
            <h1>Home library</h1>
            <p>Your books</p>
        </div>
    </div><br>

    <div class="container" align="center">
        <div>
            <a href=" <spring:url value="/library/add" />" class="btn btn-success btn-lg">
                <span class="glyphicon-info-sign glyphicon"/> Add book
            </a>
            <a href=" <spring:url value="/library" />" class="btn btn-primary btn-lg">
                <span class="glyphicon-info-sign glyphicon"/> All books
            </a>
            <a href=" <spring:url value="/library/sort" />" class="btn btn-primary btn-lg">
                <span class="glyphicon-info-sign glyphicon"/> Sort books
            </a>
        </div><br>
        <div class="input-group">
            <form:form method="post" action="/library/search">
                <input type="text" class="form-control" placeholder="Search for..." name="query">
                <input type="submit" id="btnAdd" class="btn btn-primary" value="Search"/>
            </form:form>
        </div>
    </div><br>

    <div class="container">
        <div class="row">
            <c:forEach var="book" items="${books}">
                <div class="col-sm-4">
                    <div class="panel panel-primary" style="width: 300px;">
                        <div class="panel-heading" align="center">
                            <h2>${book.title}</h2>
                        </div>
                        <div class="panel-body" align="center">
                            <img src="/resource/images/${book.id}.jpg" class="img-responsive" alt="Image" style="width: 100px; height: 142px" >
                        </div>
                        <div class="panel-footer" align="center">
                            <p><b>Author: </b>${book.author}</p>
                            <p><b>ISBN: </b>${book.ISBN}</p>
                            <p><b>Publisher: </b>${book.publisher}</p>
                            <p>
                                <a href=" <spring:url value="/library/details/${book.id}" />" class="btn btn-info">
                                    Details
                                </a>

                                <a href=" <spring:url value="/library/edit/${book.id}" />" class="btn btn-primary">
                                    Edit
                                </a>

                                <a href=" <spring:url value="library/remove/${book.id}" />" class="btn btn-danger">
                                   Remove
                                </a>

                            </p>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</body>
</html>
