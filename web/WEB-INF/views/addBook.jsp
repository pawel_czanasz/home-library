<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"/>
</head>
<body>
    <div class="jumbotron">
        <div class="container text-center">
            <h1>Home library</h1>
            <p>Add book</p>
        </div>
    </div><br>

    <div class="container">
        <form:form modelAttribute="newBook" class="form-horizontal">
            <fieldset>
                <legend>Add new Book</legend>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-lg-2" for="bookTitle">
                        <spring:message code="addBook.form.title.label"/>
                    </label>
                    <div class="col-lg-10">
                        <form:input id="bookTitle" path="title" type="text" class="form:input-large" />
                        <form:errors path="title" cssclass="error"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-lg-2" for="bookAuthor">
                        <spring:message code="addBook.form.author.label"/>
                    </label>
                    <div class="col-lg-10">
                        <form:input id="bookAuthor" path="author" type="text" class="form:input-large"/>
                        <form:errors path="author" cssclass="error"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2 col-lg-2" for="bookPublisher">
                        <spring:message code="addBook.form.publisher.label"/>
                    </label>
                    <div class="col-lg-10">
                        <form:input id="bookPublisher" path="publisher" type="text" class="form:input-large"/>
                        <form:errors path="publisher" cssclass="error"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-lg-2" for="bookISBN">
                        <spring:message code="addBook.form.isbn.label"/>
                    </label>
                    <div class="col-lg-10">
                        <form:input id="bookISBN" path="ISBN" type="text" class="form:input-large"/>
                        <form:errors path="ISBN" cssclass="error"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-lg-2" for="bookCategory">
                        <spring:message code="addBook.form.category.label"/>
                    </label>
                    <div class="col-lg-10">
                        <form:input id="bookCategory" path="category" type="text" class="form:input-large"/>
                        <form:errors path="category" cssclass="error"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-lg-2" for="bookReleaseDate">
                        <spring:message code="addBook.form.date.label"/>
                    </label>
                    <div class="col-lg-10">
                        <form:input id="bookReleaseDate" path="releaseDate" type="date" class="form:input-large"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-lg-2" for="bookNumberOfPages">
                        <spring:message code="addBook.form.pages.label"/>
                    </label>
                    <div class="col-lg-10">
                        <form:input id="bookNumberOfPages" path="numberOfPages" type="text" class="form:input-large"/>
                        <form:errors path="numberOfPages" cssclass="error"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-lg-2" for="bookLanguage">
                        <spring:message code="addBook.form.language.label"/>
                    </label>
                    <div class="col-lg-10">
                        <form:input id="bookLanguage" path="language" type="text" class="form:input-large"/>
                        <form:errors path="language" cssclass="error"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class= "col-lg-offset-2 col-lg-10">
                        <input type="submit" id="btnAdd" class="btn btn-primary" value="Save"/>
                    </div>
                </div>
                <div class="form-group">
                    <a href=" <spring:url value="/library" />" class="btn btn-danger">
                        Back
                    </a>
                </div>
            </fieldset>
        </form:form>
    </div>

</body>
</html>
