package com.homelibrary.model;


import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class Book {

    private int id;

    @NotEmpty
    private String title;

    @NotEmpty
    private String author;

    @NotEmpty
    private String publisher;


    private String releaseDate;

    @NotEmpty
    @Pattern(regexp = "[0-9]{13}")
    private String ISBN;

    private int numberOfPages;

    @NotEmpty
    private String category;

    @NotEmpty
    private String language;

    public Book() {}

    public Book(int id, String title, String author, String publisher, String releaseDate, String ISBN, int numberOfPages, String category, String language) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.releaseDate = releaseDate;
        this.ISBN = ISBN;
        this.numberOfPages = numberOfPages;
        this.category = category;
        this.language = language;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOFPages) {
        this.numberOfPages = numberOFPages;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
