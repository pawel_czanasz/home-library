package com.homelibrary.controller;

import com.homelibrary.model.Book;
import com.homelibrary.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;

@Controller
@RequestMapping("/library")
public class BookController {

    @Autowired
    BookService bookService;


    @RequestMapping()
    public String getAllBooks(Model model) throws IOException {
        model.addAttribute("books", bookService.getAllBooks());
        return "books";
    }

    @RequestMapping("/{category}")
    public String getBooksByCategory(Model model, @PathVariable String category) {
        model.addAttribute("books", bookService.getAllBooksByCategory(category));
        return "books";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String getAddBookFrom(Model model) {
        Book book = new Book();
        model.addAttribute("newBook", book);
        return "addBook";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String processAddBookFrom(@ModelAttribute("newBook") @Valid Book newBook, BindingResult result) {
        if(result.hasErrors())
            return "addBook";
        String[] suppressedFields = result.getSuppressedFields();
        if(suppressedFields.length > 0) {
            throw new RuntimeException("Próba wiązania niedozwolonych pól: "
                    + StringUtils.arrayToCommaDelimitedString(suppressedFields));
        }
        bookService.addBook(newBook);
        return "redirect:/library";
    }

    @RequestMapping(value = "/remove/{id}")
    public String removeBook(@PathVariable int id) {
        bookService.removeBook(id);
        return "redirect:/library";
    }

    @RequestMapping("/details/{id}")
    public String detailsBook(Model model, @PathVariable int id) {
        model.addAttribute("book", bookService.getBookById(id));
        return "detailsBook";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String getEditBookForm(Model model, @PathVariable int id) {
        model.addAttribute("editBook", bookService.getBookById(id));
        return "editBook";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String processEditBookForm(Model model, @ModelAttribute("editBook") @Valid Book editBook,
                                      BindingResult result, @PathVariable int id) {
        if(result.hasErrors()) {
            model.addAttribute("editBook", bookService.getBookById(id));
            return "editBook";
        }
        String[] suppressedFields = result.getSuppressedFields();
        if(suppressedFields.length > 0) {
            throw new RuntimeException("Próba wiązania niedozwolonych pól: "
                    + StringUtils.arrayToCommaDelimitedString(suppressedFields));
        }
        bookService.editBook(editBook);
        return "redirect:/library";
    }

    @RequestMapping(value="/search", method=RequestMethod.POST)
    public String search(Model model, @RequestParam("query") String query) {
        model.addAttribute("books", bookService.search(query));
        return "books";
    }

    @RequestMapping("/filter/{criteria}")
    public String getBooksByFilter(Model model, @MatrixVariable(pathVar = "criteria") Map<String, String> filterParams) {
        model.addAttribute("books", bookService.getBooksByFilter(filterParams));
        return  "books";
    }

    @RequestMapping("/sort")
    public String sortBooks(Model model) {
        model.addAttribute("books", bookService.getAllBooksSorted());
        return "books";
    }
}
