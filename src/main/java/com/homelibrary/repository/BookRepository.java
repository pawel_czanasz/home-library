package com.homelibrary.repository;


import com.homelibrary.model.Book;

import java.util.List;
import java.util.Map;

public interface BookRepository {

    List<Book> getAllBooks();
    List<Book> getBooksByCategory(String category);
    void addBook(Book book);
    void removeBook(int id);
    Book getBookById(int id);
    void editBook(Book book);
    List<Book> search(String query);
    List<Book> getBooksByFilter(Map<String, String> filterParams);
    List<Book> getAllBooksSorted();
}
