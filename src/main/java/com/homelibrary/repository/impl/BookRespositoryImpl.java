package com.homelibrary.repository.impl;

import com.homelibrary.model.Book;
import com.homelibrary.repository.BookRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class BookRespositoryImpl implements BookRepository {

    List<Book> books = new ArrayList<Book>();

    public BookRespositoryImpl() {
        books.add(createBook("Dekalog Nawałki", "Marcin Feddek",
                "sport", "9788379247264", "polski", 432, "SQN", "2016"));
        books.add(createBook("Stan futbolu", "Krzysztof Stanowski",
                "sport", "9788377002322", "polski", 272, "Czerwone i Czarne", "2016"));
        books.add(createBook("Ja, Ibra", "David Lagercrantz",
                "sport", "9788363248109", "polski", 392, "Sine Qua Non", "2012"));
        books.add(createBook("Ja, Ibra", "David Lagercrantz",
                "sport", "9788363248109", "polski", 392, "Sine Qua Non", "2012"));
        books.add(createBook("Ja, Ibra", "David Lagercrantz",
                "sport", "9788363248109", "polski", 392, "Sine Qua Non", "2012"));
        books.add(createBook("Ja, Ibra", "David Lagercrantz",
                "sport", "9788363248109", "polski", 392, "Sine Qua Non", "2012"));

    }

    public Book getBookById(int id) {
        for (Book b: books) {
            if(b.getId() == id) {
               return b;
            }
        }
        return null;
    }

    public void editBook(Book editedBook) {
        Book oldBook = getBookById(editedBook.getId());
        oldBook.setTitle(editedBook.getTitle());
        oldBook.setAuthor(editedBook.getAuthor());
        oldBook.setPublisher(editedBook.getPublisher());
        oldBook.setCategory(editedBook.getCategory());
        oldBook.setISBN(editedBook.getISBN());
        oldBook.setLanguage(editedBook.getLanguage());
        oldBook.setNumberOfPages(editedBook.getNumberOfPages());
        oldBook.setReleaseDate(editedBook.getReleaseDate());
    }

    public List<Book> search(String query) {
        query = query.toLowerCase();
        List<Book> list = new ArrayList<Book>();
        for(Book b: books) {
            if(b.getTitle().toLowerCase().contains(query) || b.getAuthor().toLowerCase().contains(query))
                list.add(b);
        }
        return list;
    }

    public List<Book> getBooksByFilter(Map<String, String> filterParams) {
        String key = filterParams.keySet().iterator().next();
        String value = filterParams.get(key);
        if(key.equals("category"))
            return getBooksByCategory(value);
        else if(key.equals("language"))
            return getBooksByLanguage(value);
        else if(key.equals("publisher"))
            return getBooksByPublisher(value);
        return null;
    }

    public List<Book> getAllBooksSorted() {
        List<Book> list = getAllBooks();
        Collections.sort(list, new Comparator<Book>() {
            public int compare(Book o1, Book o2) {
                return o1.getTitle().compareToIgnoreCase(o2.getTitle());
            }
        });
        return list;
    }

    public List<Book> getAllBooks() {
        return books;
    }

    public List<Book> getBooksByCategory(String category) {
        List<Book> list = new ArrayList<Book>();
        for (Book b: books) {
            if(b.getCategory().equalsIgnoreCase(category))
                list.add(b);
        }
        return list;
    }

    public List<Book> getBooksByLanguage(String lang) {
        List<Book> list = new ArrayList<Book>();
        for (Book b: books) {
            if(b.getLanguage().equalsIgnoreCase(lang))
                list.add(b);
        }
        return list;
    }

    public List<Book> getBooksByPublisher(String publisher) {
        List<Book> list = new ArrayList<Book>();
        for (Book b: books) {
            if(b.getPublisher().equalsIgnoreCase(publisher))
                list.add(b);
        }
        return list;
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void removeBook(int id) {
        for (Book b: books) {
            if(b.getId() == id) {
                books.remove(b);
                break;
            }
        }
    }

    private Book createBook(String title, String author, String category, String isbn, String lang, int numOfPages,
                       String publisher, String date) {
        Book book = new Book();
        book.setId(books.size()+1);
        book.setTitle(title);
        book.setAuthor(author);
        book.setCategory(category);
        book.setISBN(isbn);
        book.setLanguage(lang);
        book.setNumberOfPages(numOfPages);
        book.setPublisher(publisher);
        book.setReleaseDate(date);
        return book;
    }
}
