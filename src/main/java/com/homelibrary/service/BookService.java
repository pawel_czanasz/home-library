package com.homelibrary.service;


import com.homelibrary.model.Book;

import java.util.List;
import java.util.Map;

public interface BookService {

    void addBook(Book book);
    List<Book> getAllBooks();
    void removeBook(int id);
    List<Book> getAllBooksByCategory(String category);
    Book getBookById(int id);
    void editBook(Book book);
    List<Book> search(String query);
    List<Book> getBooksByFilter(Map<String, String> filterParams);
    List<Book> getAllBooksSorted();
}
