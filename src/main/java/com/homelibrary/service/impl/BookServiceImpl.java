package com.homelibrary.service.impl;

import com.homelibrary.model.Book;
import com.homelibrary.repository.BookRepository;
import com.homelibrary.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository bookRepository;

    public List<Book> getAllBooks() {
        return bookRepository.getAllBooks();
    }

    public List<Book> getAllBooksByCategory(String category) {
        return bookRepository.getBooksByCategory(category);
    }

    public Book getBookById(int id) {
        return bookRepository.getBookById(id);
    }

    public void editBook(Book book) {
        bookRepository.editBook(book);
    }

    public List<Book> search(String query) {
        return bookRepository.search(query);
    }

    public List<Book> getBooksByFilter(Map<String, String> filterParams) {
        return bookRepository.getBooksByFilter(filterParams);
    }

    public List<Book> getAllBooksSorted() {
        return bookRepository.getAllBooksSorted();
    }

    public void addBook(Book book) {
        bookRepository.addBook(book);
    }

    public void removeBook(int id) {
        bookRepository.removeBook(id);
    }


}
